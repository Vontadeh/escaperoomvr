﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioTrigger : MonoBehaviour {

    AudioSource audioSourceCode;
    public GameObject GameManager;
    private void Start()
    {
        audioSourceCode = GetComponent<AudioSource>();
        Debug.Log(audioSourceCode);
    }
    // Update is called once per frame

    private IEnumerator CodeTimer() {
        audioSourceCode.enabled = true;
        yield return new WaitForSeconds(3);
        audioSourceCode.enabled = false;
    }

    public void EnableSFX(){
        if (GameManager.GetComponent<GameManagement>().puzzle1b == false && GameManager.GetComponent<GameManagement>().activatedRadio == true)
        {
            StartCoroutine(CodeTimer());            
        } else {

        }
    }
}
