﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManagement : MonoBehaviour {
    
    [Header("Puzzle 1A - TVs")]
    public GameObject tv1;
    public GameObject tv2;
    public GameObject tv3;
    public GameObject tv4;
    public GameObject tv5;
    public bool puzzle1a;

    [Header("Puzzle 1B - Radio & Levers")]
    public GameObject lever1;
    public GameObject lever2;
    public GameObject lever3;
    public GameObject lever4;
    public GameObject lever5;

    public GameObject Led;
    public GameObject Door1;
    
    public GameObject  B1Light;
    public GameObject  BatteryUI;
    public GameObject  radioTrigger;
    public GameObject  LedDoor1;
    public GameObject  Lamp;

    public bool canActivateRadio;  
    public bool activatedRadio;

    public bool puzzle1b;

    [Header("Puzzle 2A - Safe & Batteries")]
    public GameObject SafeDoor;  
    public GameObject Batteries;
    public GameObject  SafeBoxDial1;
    public GameObject  SafeBoxDial2;
    public GameObject  SafeBoxDial3;
    public GameObject radioBatteries;
    public AudioSource extraSFXRadio;
    public bool puzzle2a;

    [Header("Puzzle 2B - Valves")]
    public GameObject valveDoor;
    public GameObject valve1;
    public GameObject valve2;
    public GameObject valve3;
    public GameObject Door2;
    public GameObject  LedDoor2;
    public bool puzzle2b;
    
    [Header("Canvas")]
    public GameObject winCanvas; 
    public GameObject GOCanvas;
    public GameObject EscapeDoor;

    [Header("Countdown")]
    public int min;
    public int sec;
    public Text CountdownText;
    public Text WinTimeText;
    public bool canSubtract;
    
    bool PressAnyKey;

    [Header("SFX")]

    public AudioSource FeedbackSliders;
    public AudioSource FeedbackValves;
    public AudioSource FeedbackLamp;
    public AudioSource FeedbackSwitch;
    public AudioSource FeedbackButton;
    public AudioSource FeedbackSafe;
    public AudioSource FeedbackSafeDoor;
    public AudioSource FeedbackBatteries;
    public AudioSource FeedbackBatteriesRadio;
    public AudioSource FeedbackOpenValveDoor;
    public AudioSource FeedbackTV;
    public AudioSource FeedbackDoorRod1;
    public AudioSource FeedbackDoorRod2;
    public AudioSource FeedbackDoorFinal;
    public AudioSource BombsBackgrounds;
    public AudioSource PeopleRunnings;
    public GameObject DustParticles;
            
    bool OpenValveDoorOnce; 
    bool SafeOnce;
    bool BatteriesOnce;
    bool Door1Once;
    bool Door2Once;
    bool DoorFinalOnce;
    bool BombsSFXOnce;
    bool PeopleRunningOnce;



    [Header("Misc")]
    public GameObject Player; 
	// Use this for initialization
	void Start () {
        Time.timeScale = 0.0f;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
        PressAnyKey = false;
        canSubtract = false;
	}
	
	// Update is called once per frame
    void Update(){
        if (canSubtract == true){
            CountdownText.GetComponent<Text>().text = min.ToString("00") + ":" +  sec.ToString("00");
            StartCoroutine(Countdown());
        }
        if (Input.GetMouseButtonDown(0) && PressAnyKey == false){
            Time.timeScale = 1.0f;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            PressAnyKey = true;
            canSubtract = true;
            BombsBackgrounds.enabled = true;
            PeopleRunnings.enabled = true;
         }
    }

    // ROTATE LAMP
    public void RotateLampLeft(){
            FeedbackLamp.GetComponent<AudioSource>().Play();
            if (Lamp.GetComponent<Animator>().GetInteger("Rotation") < 0){
                Lamp.GetComponent<Animator>().SetInteger("Rotation", 7);
                } else {
                Lamp.GetComponent<Animator>().SetInteger("Rotation", ((Lamp.GetComponent<Animator>().GetInteger("Rotation")) - 1));

            }
  
    }
    public void RotateLampRight(){
            FeedbackLamp.GetComponent<AudioSource>().Play();
            if (Lamp.GetComponent<Animator>().GetInteger("Rotation") > 7){
                Lamp.GetComponent<Animator>().SetInteger("Rotation", 0);
                } else {
                Lamp.GetComponent<Animator>().SetInteger("Rotation", ((Lamp.GetComponent<Animator>().GetInteger("Rotation")) + 1));
            }
  
    }

    // PUZZLE 1B
    public void CheckLeverPuzzle() {
        FeedbackButton.GetComponent<AudioSource>().Play();
        if (lever1.GetComponent<Animator>().GetInteger("LeverPosition") == 1 &&
            lever2.GetComponent<Animator>().GetInteger("LeverPosition") == 2 &&
            lever3.GetComponent<Animator>().GetInteger("LeverPosition") == 0 &&
            lever4.GetComponent<Animator>().GetInteger("LeverPosition") == 2 &&
            lever5.GetComponent<Animator>().GetInteger("LeverPosition") == 1 && puzzle1b == false && activatedRadio == true) {
            OpenDoor1();
            LedDoor2.GetComponent<Renderer>().materials[1].color = Color.green;
            puzzle1b = true;
        
        } else {
        }
    }

    public void CheckLed(){
        FeedbackSliders.GetComponent<AudioSource>().Play();
        if (lever1.GetComponent<Animator>().GetInteger("LeverPosition") == 1 &&
            lever2.GetComponent<Animator>().GetInteger("LeverPosition") == 2 &&
            lever3.GetComponent<Animator>().GetInteger("LeverPosition") == 0 &&
            lever4.GetComponent<Animator>().GetInteger("LeverPosition") == 2 &&
            lever5.GetComponent<Animator>().GetInteger("LeverPosition") == 1) {
            Led.GetComponent<Renderer>().materials[1].color = Color.green; 
        } else {
         Led.GetComponent<Renderer>().materials[1].color = Color.red;
        }
    }
    public void PlayRadioCode(){
        radioTrigger.gameObject.GetComponent<RadioTrigger>().EnableSFX();
    }
    void OpenDoor1() {
        Door1.gameObject.GetComponent<Animator>().SetBool("Opened",true);
            if (Door1Once == false){
            FeedbackDoorRod1.GetComponent<AudioSource>().Play();
            Door1Once = true;
        }
    }
    void OpenDoor2() {
        Door2.gameObject.GetComponent<Animator>().SetBool("Opened",true);
            if (Door2Once == false){
            FeedbackDoorRod2.GetComponent<AudioSource>().Play();
            Door2Once = true;
        }
    }
    private void ResetLeverPuzzle()
    {
        lever1.GetComponent<Animator>().SetInteger("LeverPosition", 1);
        lever2.GetComponent<Animator>().SetInteger("LeverPosition", 1);
        lever3.GetComponent<Animator>().SetInteger("LeverPosition", 1);
        lever4.GetComponent<Animator>().SetInteger("LeverPosition", 1);
        lever5.GetComponent<Animator>().SetInteger("LeverPosition", 1);
    }

    // PUZZLE 1A

    public void TurnOnLight(){
        FeedbackSwitch.GetComponent<AudioSource>().Play();
        if (B1Light.activeInHierarchy == false){
            B1Light.SetActive(true);
        } else {
            B1Light.SetActive(false);
        }
            
    }

    public void OpenSafeBox(){
        if (SafeBoxDial1.GetComponent<Animator>().GetInteger("DialPosition") == 1 &&
            SafeBoxDial2.GetComponent<Animator>().GetInteger("DialPosition") == 4 &&
            SafeBoxDial3.GetComponent<Animator>().GetInteger("DialPosition") == 9) {
            SafeDoor.GetComponent<Animator>().SetBool("Opened", true);
            puzzle2a = true;
            Batteries.SetActive(true);
            if (SafeOnce == false){
            FeedbackSafeDoor.GetComponent<AudioSource>().Play();
            SafeOnce = true;
        }
        }
    }
    public void ShowBatteryUI(){
    BatteryUI.SetActive(true);
}
    public void RemoveBatteryUI(){
    BatteryUI.SetActive(false);
    }
    public void PickupBatteries(){
        FeedbackBatteries.GetComponent<AudioSource>().Play();
        canActivateRadio = true;
        Batteries.SetActive(false);
    }
    public void ActivateRadio(){
        extraSFXRadio.enabled = true;
        radioBatteries.SetActive(true);
        activatedRadio = true;
        FeedbackBatteriesRadio.GetComponent<AudioSource>().Play();
    }
    public void OpenValveDoor(){
        valveDoor.GetComponent<Animator>().SetBool("Opened", true);
                
         if (OpenValveDoorOnce == false){
            FeedbackOpenValveDoor.GetComponent<AudioSource>().Play();
            OpenValveDoorOnce = true;
        }
    }

    IEnumerator BombsBackground(){
            BombsBackgrounds.GetComponent<AudioSource>().Play();
            InvokeRepeating("ParticlesAndShake", 6.25f, 16f);
            yield return new WaitForSeconds(0f);
    }

    public void PeopleRunning(){
            PeopleRunnings.GetComponent<AudioSource>().Play();
    }
    IEnumerator Countdown(){

        if (min < 5){
            if(PeopleRunningOnce == false){
                PeopleRunning();
                PeopleRunningOnce = true;
            }  

            if (min < 2 && BombsSFXOnce == false){
                StartCoroutine("BombsBackground");
                BombsSFXOnce = true;
            }
        }
        canSubtract = false;
        yield return new WaitForSeconds(1f);
        if (sec > 0) {
            sec = sec - 1;
            canSubtract = true;
        } else if ( min > 0) {
            min = min - 1;
            sec = 59;
            canSubtract = true;
        } else {
           GameOver();
        }
     } 

void ParticlesAndShake(){
    DustParticles.GetComponent<ParticleSystem>().Play();
    GetComponent<CameraShakee>().ShakeIt();

}
    public void GameOver(){
        GOCanvas.SetActive(true);
        Cursor.visible = true;
        Player.GetComponent<FirstPersonController>().enabled = false;
        Cursor.lockState = CursorLockMode.None;
    }

    public void ChangeTVImage(GameObject TV){
        FeedbackTV.GetComponent<AudioSource>().Play();
        if (TV.GetComponent<Animator>().GetInteger("Cycle") == 0){
        TV.transform.GetChild(0).transform.gameObject.SetActive(false);
        TV.transform.GetChild(1).transform.gameObject.SetActive(false);
        TV.transform.GetChild(2).transform.gameObject.SetActive(true);
        TV.transform.GetChild(3).transform.gameObject.SetActive(false);
        TV.transform.GetChild(4).transform.gameObject.SetActive(false);
        } else if (TV.GetComponent<Animator>().GetInteger("Cycle") == 1){
        TV.transform.GetChild(0).transform.gameObject.SetActive(false);
        TV.transform.GetChild(1).transform.gameObject.SetActive(true);
        TV.transform.GetChild(2).transform.gameObject.SetActive(false);
        TV.transform.GetChild(3).transform.gameObject.SetActive(false);
        TV.transform.GetChild(4).transform.gameObject.SetActive(false);
        } else if (TV.GetComponent<Animator>().GetInteger("Cycle") == 2){
        TV.transform.GetChild(0).transform.gameObject.SetActive(true);
        TV.transform.GetChild(1).transform.gameObject.SetActive(false);
        TV.transform.GetChild(2).transform.gameObject.SetActive(false);
        TV.transform.GetChild(3).transform.gameObject.SetActive(false);
        TV.transform.GetChild(4).transform.gameObject.SetActive(false);
        } else if (TV.GetComponent<Animator>().GetInteger("Cycle") == 3){
        TV.transform.GetChild(0).transform.gameObject.SetActive(false);
        TV.transform.GetChild(1).transform.gameObject.SetActive(false);
        TV.transform.GetChild(2).transform.gameObject.SetActive(false);
        TV.transform.GetChild(3).transform.gameObject.SetActive(false);
        TV.transform.GetChild(4).transform.gameObject.SetActive(true);
        } else if (TV.GetComponent<Animator>().GetInteger("Cycle") == 4){
        TV.transform.GetChild(0).transform.gameObject.SetActive(false);
        TV.transform.GetChild(1).transform.gameObject.SetActive(false);
        TV.transform.GetChild(2).transform.gameObject.SetActive(false);
        TV.transform.GetChild(3).transform.gameObject.SetActive(true);
        TV.transform.GetChild(4).transform.gameObject.SetActive(false);
        }
        CheckTVPuzzle();
    }

    public void CheckTVPuzzle(){
        if(tv1.GetComponent<Animator>().GetInteger("Cycle") == 2 && tv2.GetComponent<Animator>().GetInteger("Cycle") == 1 && tv3.GetComponent<Animator>().GetInteger("Cycle") == 0 && tv4.GetComponent<Animator>().GetInteger("Cycle") == 4 &&
        tv5.GetComponent<Animator>().GetInteger("Cycle") == 3){
            OpenValveDoor();
       
            puzzle1a = true;

        }  
    }

    public void CheckValves(){
        FeedbackValves.GetComponent<AudioSource>().Play();
        if (valve1.GetComponent<Animator>().GetBool("Valve") == true && valve2.GetComponent<Animator>().GetBool("Valve") == true && valve3.GetComponent<Animator>().GetBool("Valve") == true){
        puzzle2b = true;
        OpenDoor2();
        LedDoor1.GetComponent<Renderer>().materials[1].color = Color.green;
        }
    }

    public void CheckEscapeDoor(){
        if(puzzle1a == true && puzzle1b == true && puzzle2a == true && puzzle2b == true){
            EscapeDoor.GetComponent<Animator>().SetBool("Opened", true);
            if (DoorFinalOnce == false){
             FeedbackDoorFinal.GetComponent<AudioSource>().Play();
             DoorFinalOnce = true;
            }
        }
        

    }

    public void RestartGame(){
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        GOCanvas.SetActive(false);
        winCanvas.SetActive(false);
        Cursor.visible = false;
        Player.GetComponent<FirstPersonController>().enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void WinGame(){
        WinTimeText.GetComponent<Text>().text = (9-min).ToString("00") + ":" + (59-sec).ToString("00");
        canSubtract = false;
        winCanvas.SetActive(true);
        Cursor.visible = true;
        Player.GetComponent<FirstPersonController>().enabled = false;
        Cursor.lockState = CursorLockMode.None;
    }

}
