﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRaycast : MonoBehaviour {
    // Use this for initialization
    public GameObject Crosshair;
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
            Crosshair.SetActive(false);
            Vector3 fwd = transform.TransformDirection(Vector3.forward);
            RaycastHit hit;
            Physics.Raycast(transform.position, fwd, out hit, 2f);
            Debug.DrawLine(transform.position, hit.transform.position, Color.red, 0.2f);
            Crosshair.SetActive(false);
            //CROSSHAIR
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Interactible")){
                Crosshair.SetActive(true);
            }
            //RIGHT CLICK
        if (Input.GetMouseButtonDown(1)){
            //SAFE
            if (hit.transform.tag == "SafeDial")
            {   
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().FeedbackSafe.GetComponent<AudioSource>().Play();
                hit.transform.GetComponent<Animator>().SetInteger("DialPosition", ((hit.transform.GetComponent<Animator>().GetInteger("DialPosition")) -1));
                    if (hit.transform.GetComponent<Animator>().GetInteger("DialPosition") <= 0){
                        hit.transform.GetComponent<Animator>().SetInteger("DialPosition", 9);    
                }
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().OpenSafeBox();
            }

            //TV
            if (hit.transform.tag == "TV")
            {
                if(hit.transform.GetComponent<Animator>().GetInteger("Cycle") > 0){
                    hit.transform.GetComponent<Animator>().SetInteger("Cycle", (hit.transform.GetComponent<Animator>().GetInteger("Cycle")) - 1);
                    GameObject.Find("GameManager").transform.GetComponent<GameManagement>().ChangeTVImage(hit.transform.gameObject);
                } else {
                    hit.transform.GetComponent<Animator>().SetInteger("Cycle", 4);
                    GameObject.Find("GameManager").transform.GetComponent<GameManagement>().ChangeTVImage(hit.transform.gameObject);
                }
                
            }

            if (hit.transform.tag == "Lever")
            {
            hit.transform.GetComponent<Animator>().SetInteger("LeverPosition", (Mathf.Clamp(((hit.transform.GetComponent<Animator>().GetInteger("LeverPosition")) - 1), 0, 2)));
            GameObject.Find("GameManager").transform.GetComponent<GameManagement>().CheckLed();
            }
            //LAMP
           if (hit.transform.tag == "Cylinder")
            {
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().RotateLampLeft();
                       
            }
        }
        if (Input.GetMouseButtonDown(0)){
            
            if (hit.transform.tag == "Cylinder")
            {
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().RotateLampRight();
                       
            }

            if (hit.transform.tag == "Radio")
            {
              GameObject.Find("GameManager").transform.GetComponent<GameManagement>().PlayRadioCode();
            }

             //VALVES
            if (hit.transform.tag == "Valve")
            {
               if (hit.transform.position.x > -3.5 && hit.transform.position.x < 3.5){
                    hit.transform.GetComponent<Animator>().SetBool("Valve", true);    
                    GameObject.Find("GameManager").transform.GetComponent<GameManagement>().CheckValves();
               }
                
            }

            if (hit.transform.tag == "OpenValvesDoor")
            {
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().CheckValves();
            }
             if (hit.transform.tag == "EscapeValve")
            {
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().CheckEscapeDoor();
            }
             //BATTERIES

            if (hit.transform.tag == "RadioBatteries" && GameObject.Find("GameManager").transform.GetComponent<GameManagement>().canActivateRadio == true)
            {
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().ActivateRadio();
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().RemoveBatteryUI();
            }
            if (hit.transform.tag == "Batteries")
            {
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().PickupBatteries();
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().ShowBatteryUI();
                
            }
            
            //LEVERS
            if (hit.transform.tag == "LeverButton")
            {

                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().CheckLeverPuzzle();
                hit.transform.GetComponent<Animator>().transform.GetComponent<Animator>().SetTrigger("Pushed");
            }

            if (hit.transform.tag == "Lever")
            {
             hit.transform.GetComponent<Animator>().SetInteger("LeverPosition", (Mathf.Clamp(((hit.transform.GetComponent<Animator>().GetInteger("LeverPosition")) + 1), 0, 2)));
            GameObject.Find("GameManager").transform.GetComponent<GameManagement>().CheckLed();

            }

            if (hit.transform.tag == "Button")
            {
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().TurnOnLight();
                 hit.transform.GetComponent<Animator>().transform.GetComponent<Animator>().SetBool("isOn", !(hit.transform.GetComponent<Animator>().transform.GetComponent<Animator>().GetBool("isOn")));
                 
            }

        //TV

            if (hit.transform.tag == "TV")
            {
                if(hit.transform.GetComponent<Animator>().GetInteger("Cycle") < 4){
                    hit.transform.GetComponent<Animator>().SetInteger("Cycle", (hit.transform.GetComponent<Animator>().GetInteger("Cycle")) + 1);
                    GameObject.Find("GameManager").transform.GetComponent<GameManagement>().ChangeTVImage(hit.transform.gameObject);
                } else {
                    hit.transform.GetComponent<Animator>().SetInteger("Cycle", 0);
                    GameObject.Find("GameManager").transform.GetComponent<GameManagement>().ChangeTVImage(hit.transform.gameObject);
                }
                
            }

        if (hit.transform.tag == "RadioLid")
            {
                hit.transform.GetComponent<Animator>().SetBool("Opened", true);
            }

         //SAFE
            if (hit.transform.tag == "SafeDial")
            {
                GameObject.Find("GameManager").transform.GetComponent<GameManagement>().FeedbackSafe.GetComponent<AudioSource>().Play();
                    if (hit.transform.GetComponent<Animator>().GetInteger("DialPosition") >= 10){
                    hit.transform.GetComponent<Animator>().SetInteger("DialPosition", 1);
                    } else{
                        hit.transform.GetComponent<Animator>().SetInteger("DialPosition", ((hit.transform.GetComponent<Animator>().GetInteger("DialPosition")) + 1));
                    }
                     GameObject.Find("GameManager").transform.GetComponent<GameManagement>().OpenSafeBox();

                }
        //LAMP

      }
    }
}