﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
public class WinGame : MonoBehaviour
{
    public GameObject GameManager; 
    // Start is called before the first frame update
    public void OnTriggerEnter(Collider other){
        if (other.transform.tag == "Player"){
            GameObject.Find("GameManager").transform.GetComponent<GameManagement>().WinGame();
        }
    }
}
